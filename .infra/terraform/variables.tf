variable "main_count" {
  type    = number
  default = 1
}

variable "worker_count" {
  type    = number
  default = 3
}

variable "do_token" {
  type = string
}

variable "dns_zone" {
  type    = string
  default = "devops.rebrain.srwx.net"
}

variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}


