resource "digitalocean_droplet" "main" {
  image    = "centos-7-x64"
  name     = "main-${count.index+1}"
  count    = var.main_count
  region   = "fra1"
  size     = "s-1vcpu-2gb"
  ssh_keys = [data.digitalocean_ssh_key.eaob.fingerprint, data.digitalocean_ssh_key.rebrain.id ]
  tags     = [digitalocean_tag.droplet.id]

    provisioner "remote-exec" {
    inline = [
       "echo 'OK'"
       ]

    connection {
      host =  self.ipv4_address
      type     = "ssh"
      user     = "root"
      private_key = "${file("/root/id_rsa_DO")}"
    }
  }
}

resource "digitalocean_droplet" "worker" {
  image    = "centos-7-x64"
  name     = "worker-${count.index+1}"
  count    = var.worker_count
  region   = "fra1"

  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.eaob.fingerprint, data.digitalocean_ssh_key.rebrain.id ]
  tags     = [digitalocean_tag.droplet.id]

    provisioner "remote-exec" {
    inline = [
       "echo 'OK'"
       ]

    connection {
      host =  self.ipv4_address
      type     = "ssh"
      user     = "root"
      private_key = "${file("/root/id_rsa_DO")}"
    }
  }
}

data "digitalocean_ssh_key" "rebrain" {
  name     = "REBRAIN.SSH.PUB.KEY"
}

data "digitalocean_ssh_key" "eaob" {
  name     = "eaob"
}

data "aws_route53_zone" "devops" {
  name = var.dns_zone
}

resource "aws_route53_record" "prometheus" {
  zone_id = data.aws_route53_zone.devops.zone_id
  name    = "prometheus"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.main[0].ipv4_address}"]
}

resource "aws_route53_record" "grafana" {
  zone_id = data.aws_route53_zone.devops.zone_id
  name    = "grafana"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.main[0].ipv4_address}"]
}

resource "digitalocean_tag" "droplet" {
  name = "eaob"
}

resource "local_file" "infrastructure" {
  count    = var.worker_count
  content  = templatefile("${path.module}/inventory.tmpl",
  {
  ip       = digitalocean_droplet.main.*.ipv4_address,
  hostname = digitalocean_droplet.main.*.name,
  ip_worker = digitalocean_droplet.worker.*.ipv4_address,
  hostname_worker = digitalocean_droplet.worker.*.name,
  }
 )
  filename = "${path.module}/inventory.txt"
}

