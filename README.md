**Кластер docker swarm с предустановленной системой мониторинга и оповещений (Prometheus, Grafana, Alertmanager)**

Для создания нового кластера docker swarm с предустановленной системой мониторинга и оповещений достаточно запустить pipeline.
Для использования в production необходимо настроить backend в файле .infra/terraform/providers.tf для хранения состояния и функционирования блокировок terraform.


**Установка и настройка**

- склонировать репозиторий
- в настройках CI/CD добавить следующие переменные и присвоить им значения:
```
    GITLAB_aws_access_key - ключ доступа AWS 
    GITLAB_aws_secret_key - ключ секрета AWS
    GITLAB_do_token       - ключ доступа digitalocean
```
- при необходимости добавить private_key для доступа к созданным дроплетам
- в файле .infra/terraform/variables.tf изменить дефолтные значения main_count и worker_count или добавить свой файл со значениями переменных для указания количества master и worker нод в кластере соответственно. Изменить значение dns_zone на имя своей зоны ДНС.
- заменить путь к закрытому ключу на использование своего ранее созданного private_key в файле .infra/terraform/main.tf
- запускать pipeline необходимо на раннере с terraform и ansible


**Доступ**


В примере панель prometheus доступна по адресу - https://prometheus.devops.rebrain.srwx.net   (admin/test)

Панель grafana доступна по адресу - https://grafana.devops.rebrain.srwx.net   (admin/test)



Для просмотра dashboard: Dashboards -> Manage и выбрать Docker Prometheus Monitoring

Пароль доступа к grafana - .infra/ansible/files/monitoring-stack/grafana/config.monitoring

Пароль доступа к prometheus - .infra/ansible/files/monitoring-stack/nginx-certbot/data/nginx/.htpasswd

Настройка оповещений - .infra/ansible/files/monitoring-stack/prometheus/alert.rules

Конфигурация Slack - .infra/ansible/files/monitoring-stack/alertmanager/config.yml






